# Copyright 2021 Benedikt Neuffer <ogelpre@itfriend.de>
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit unpacker udev

DESCRIPTION="sane drivers for brother ds series scanner"
HOMEPAGE="https://www.brother.de/support/ds-620/downloads"

SRC_URI="https://download.brother.com/welcome/dlf100976/${PN}_${PV}-1_amd64.deb"

RESTRICT="mirror"
LICENSE="Brother"
SLOT="0"
KEYWORDS="~amd64"

PDEPEND="
		media-gfx/sane-backends
"

S="${WORKDIR}"

src_prepare() {
	unpack usr/share/doc/libsane-dsseries/changelog.Debian.gz

	echo "dsseries" > dsseries.conf

	default
}

src_install() {
	dodoc usr/share/doc/libsane-dsseries/copyright changelog.Debian

	insinto /etc/sane.d
	doins usr/lib/tmp_DSDriver/dsseries.conf

	insinto /etc/sane.d/dll.d
	doins dsseries.conf

	insinto /usr/lib64/sane
	insopts -m0755
	doins usr/lib/tmp_DSDriver/x64/*
	dosym "libsane-dsseries.so.1.0.17" "usr/lib64/sane/libsane-dsseries.so.1"
	dosym "libsane-dsseries.so.1" "usr/lib64/sane/libsane-dsseries.so"

	udev_dorules usr/lib/tmp_DSDriver/50-Brother_DSScanner.rules
}

pkg_postinst()
{
	udev_reload

	ewarn "you have to restart sane"
}
