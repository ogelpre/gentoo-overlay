# Copyright 2021 Benedikt Neuffer <ogelpre@itfriend.de>
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop git-r3

DESCRIPTION="Eclipse based LDAP browser and directory client."
HOMEPAGE="https://directory.apache.org/studio/"

PRODUCTNAME="${PN%%-*}"
FOUNDATION="${PRODUCTNAME%%_*}"
SOFTWARE="${PRODUCTNAME##*_}"
PROJECT_TMP="${PRODUCTNAME%_*}"
PROJECT="${PROJECT_TMP#*_}"

ALPHA_VERSION="${PV##*_alpha}"
VERSION_TMP="${PV%%_*}"
RELEASE_DATE="${VERSION_TMP##*.}"
VERSION="${VERSION_TMP%.*}"
unset VERSION_TMP

SRC_URI="amd64? ( https://dlcdn.${FOUNDATION}.org/${PROJECT}/${SOFTWARE}/${VERSION}.v${RELEASE_DATE}-M${ALPHA_VERSION}/${FOUNDATION^}${PROJECT^}${SOFTWARE^}-${VERSION}.v${RELEASE_DATE}-M${ALPHA_VERSION}-linux.gtk.x86_64.tar.gz )"

EGIT_REPO_URI="https://aur.archlinux.org/apachedirectorystudio.git/"
EGIT_COMMIT="2e7e6e755584c1fc7ac07a88e96540588f6ab818"
EGIT_CHECKOUT_DIR="${WORKDIR}/arch"

RESTRICT="mirror"
LICENSE="Apache-2.0 BSD CDDL EPL-1.0 ICU4J MIT W3C"
SLOT="2"
KEYWORDS="~amd64"

DEPEND="
	dev-util/desktop-file-utils
"

PDEPEND="
	app-crypt/libsecret
	dev-libs/glib
	dev-libs/libgcrypt
	dev-libs/libgpg-error
	dev-libs/libffi
	dev-libs/libpcre
	sys-apps/util-linux
	sys-libs/zlib
	virtual/jre:11
"

S="${WORKDIR}/${FOUNDATION^}${PROJECT^}${SOFTWARE^}"

src_unpack() {
	default

	git-r3_fetch
	git-r3_checkout
}

src_prepare() {
	default

	sed -i "s/.png//" "${WORKDIR}/arch/${FOUNDATION}${PROJECT}${SOFTWARE}.desktop"
}

src_install() {
	mkdir -p "${D}/opt/${FOUNDATION}${PROJECT}${SOFTWARE}"
	cp --recursive "${S}" "${D}/opt/" || die "Install failed!"

	dosym "../../opt/${FOUNDATION^}${PROJECT^}${SOFTWARE^}/${FOUNDATION^}${PROJECT^}${SOFTWARE^}" "/usr/bin/${FOUNDATION}${PROJECT}${SOFTWARE}"

	doicon "${WORKDIR}/arch/${FOUNDATION}${PROJECT}${SOFTWARE}.png"
	domenu "${WORKDIR}/arch/${FOUNDATION}${PROJECT}${SOFTWARE}.desktop"
}
