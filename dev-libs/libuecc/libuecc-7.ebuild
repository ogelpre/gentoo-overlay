# Copyright 2021 Benedikt Neuffer <ogelpre@itfriend.de>
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit cmake

DESCRIPTION="Very small Elliptic Curve Cryptography library"
HOMEPAGE="https://projects.universe-factory.net/projects/fastd/wiki"

SRC_URI="https://git.universe-factory.net/${PN}/snapshot/${PN}-${PV}.zip"

RESTRICT="mirror"
LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64"

BDEPEND="
	app-arch/unzip
	dev-util/cmake
"

S="${WORKDIR}/${P}"
