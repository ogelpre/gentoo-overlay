# Copyright 2021 Benedikt Neuffer <ogelpre@itfriend.de>
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit linux-info

DESCRIPTION="alfred is a user space daemon to flood the network with useless data"
HOMEPAGE="https://www.open-mesh.org/projects/alfred/wiki"

SRC_URI="https://downloads.open-mesh.org/batman/stable/sources/${PN}/${PN}-${PV}.tar.gz"

RESTRICT="mirror"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="caps gpsd vis"

RDEPEND="
	>=dev-libs/libnl-3
	gpsd? ( sci-geosciences/gpsd )
	caps? ( sys-libs/libcap )
"
DEPEND="${RDEPEND}"

S="${WORKDIR}/${P}"

pkg_setup() {
	if ( linux_config_exists && linux_chkconfig_present CONFIG_IPV6 ); then
		ewarn "You need IPv6 support in Kernel."
	fi
}

src_prepare() {
	sed -i "s|PREFIX =|PREFIX := |" "${S}/Makefile"
	default
}

src_compile() {
	MKOPT=""
	use gpsd || MKOPT="${MKOPT} CONFIG_ALFRED_GPSD=n"
	use vis || MKOPT="${MKOPT} CONFIG_ALFRED_VIS=n"
	use caps || MKOPT="${MKOPT} CONFIG_ALFRED_CAPABILITIES=n"

	emake ${MKOPT} || die "emake failed";
}

src_install() {
	MKOPT=""
	use gpsd || MKOPT="${MKOPT} CONFIG_ALFRED_GPSD=n"
	use vis || MKOPT="${MKOPT} CONFIG_ALFRED_VIS=n"
	use caps || MKOPT="${MKOPT} CONFIG_ALFRED_CAPABILITIES=n"

	emake ${MKOPT} PREFIX="${EPREFIX}"/usr DESTDIR="${D}" install || die "emake install failed";

	doman man/${PN}.8
	dodoc CHANGELOG.rst README.rst

	newinitd "${FILESDIR}/${PN}-2016.3.initd" ${PN}
}
