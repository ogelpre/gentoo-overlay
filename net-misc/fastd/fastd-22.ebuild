# Copyright 2015-2016 Benedikt Neuffer <ogelpre@itfriend.de>
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson

DESCRIPTION="fastd is a Fast and Secure Tunneling Daemon"
HOMEPAGE="https://github.com/NeoRaider/fastd/wiki"

SRC_URI="https://github.com/NeoRaider/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${PN}-v${PV}.tar.gz"

RESTRICT="mirror"
LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="caps lto l2tp nacl openssl status-socket"

BDEPEND="app-arch/unzip"
RDEPEND="
	caps? ( sys-libs/libcap )
	=dev-libs/libuecc-7*
	!nacl? ( dev-libs/libsodium )
	nacl? ( dev-python/libnacl )
	openssl? ( dev-libs/openssl:* )
	status-socket? ( dev-libs/json-c )
"
DEPEND="
	${RDEPEND}
"
BDEPEND="
	dev-ruby/pkg-config
	sys-devel/bison
"

S="${WORKDIR}/${P}"

src_configure() {
	local emesonargs=(
		$(meson_feature caps capabilities)
		$(meson_option nacl use_nacl)
		$(meson_feature l2tp offload_l2tp)
		$(meson_feature status-socket)
	)
	meson_src_configure
}

src_install() {
	doman doc/fastd.1
	dodoc README.md
	newinitd "${FILESDIR}/fastd-0.17.initd" fastd
	keepdir /etc/fastd

	meson_src_install
}
